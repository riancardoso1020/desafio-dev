document.addEventListener('keydown', function(e) {
    if(e.key == "Enter"){
      document.getElementById("botao").click();
    }
});


function calcularJogadoresLivres(){

var T = document.getElementById('T').value;
if(T< 1 || T>24){
    alert("Quantidade de segundos restantes até o fim da jogada tem que ser entre 1 e 24.")
    return erro;
}

var Q = document.getElementById('Q').value;
if(Q< 2 || Q>5 ){
    alert("Quantidade de jogadores em quadra tem que ser entre 2 e 5.")
    return erro;
}

var X = document.getElementById('X').value;
if(X< 1 || X> (Q-1) ){
    alert("Tempo de reposicionamento dos jogadores após o passe tem que ser entre 1 e (Quantidade de jogadores em quadra - 1).")
    return erro;
}

if((T%1 || Q%1 || X%1) != 0){
    alert("Só são válidos números inteiros!")
    return erro;
}

var Passes = []

for(var i=0;i<T;i++){
    Passes [Passes.length] = 0
}


var result = 0
for(var i=0;i<T;i++){
    Passes[i] = Q-1+Passes[i]
    result = Passes[i] + result

    for(var j=1;j<=(X-1);j++){

        if(i+j < T){
            Passes[i+j] = ((-1)+(Passes[i+j]))
        }

    }

}


document.getElementById("resultado").innerHTML = result

}
